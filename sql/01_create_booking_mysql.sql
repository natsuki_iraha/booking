-- データベース作成
DROP DATABASE IF EXISTS booking;
CREATE DATABASE booking;
USE booking;

-- 予約対象マスタ
DROP TABLE IF EXISTS `booking_target`;
CREATE TABLE `booking_target` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `information` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 予約データ
DROP TABLE IF EXISTS `booking_data`;
CREATE TABLE `booking_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_code` varchar(30) DEFAULT NULL,
  `start_datetime` varchar(20) DEFAULT NULL,
  `end_datetime` varchar(20) DEFAULT NULL,
  `user_name` varchar(20) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `description` text,
  `user_section_name` varchar(20) DEFAULT NULL,
  `user_phone_number` varchar(20) DEFAULT NULL,
  `member_count` varchar(2) DEFAULT NULL,
  `update_datetime` varchar(20) DEFAULT NULL,
  `deleted_flag` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- db_userにbookingを利用する権限を追加
GRANT SELECT,INSERT,UPDATE,DELETE ON `booking`.* TO 'db_user'@'%';

-- 予約対象マスタのテスト用デフォルトデータ
INSERT INTO `booking_target` (`id`, `code`, `name`, `information`) VALUES
(1, 'room_a', '会議室Ａ', '会議室Ａには１０人座れます。\nホワイトボードが１枚あり、小規模な打ち合わせに利用できます。'),
(2, 'room_b', '会議室Ｂ', '会議室Ｂには３０人座れます。\nホワイトボードが４枚あり、プロジェクターも備え付けのため、\nグループワークのある研修などにも対応可能です。'),
(3, 'projector_01', 'プロジェクター１号', 'エプソン\n高輝度のため電気をつけたままでも問題なく使用できます。');
