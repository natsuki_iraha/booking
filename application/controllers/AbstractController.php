<?php

/**
 * Controllerクラスの抽象クラス
 *
 * Viewのセットアップと管理、Json出力などのメソッドを持っています。
 *
 * @access abstract
 */
abstract class AbstractController {

	protected $con_name;
	protected $act_name;
	protected $view;

	/**
	 * コンストラクタ
	 *
	 * リクエストパラメータのController名とAction名をプロパティにセットします。
	 *
	 * @access protected
	 * @param string $con_name リクエストパラメータのController名
	 * @param string $act_name リクエストパラメータのAction名
	 */
	protected function __construct(string $con_name, string $act_name) {
		$this->con_name = $con_name;
		$this->act_name = $act_name;
	}

	/**
	 * Viewのセットアップを行います。
	 *
	 * ControllerとActionに対応するViewを生成してプロパティにセットして<br />
	 * そのViewへcssやjsのリンクをセットします。<br />
	 * 「reset.css」「common.css」「utility.jsは共通でセットし、＜br />
	 * オプション「$include」によりController名と同名のcssとjsをセットします。
	 *
	 * @access protected
	 * @param string $other_phtml 命名規則外の場合のViewファイル名、省略時null
	 * @param bool $include Controller名と同名のcssとjsを読み込むかどうか、省略時true
	 */
	protected function setupView(string $other_phtml = null, bool $include = true) {
		$this->view = new View($this->con_name, $this->act_name, $other_phtml);
		$this->view->addCss('reset.css');
		$this->view->addCss('common.css');
		$this->view->addJs('utility.js');
		if ($include) {
			$this->view->addCss("{$this->con_name}.css");
			$this->view->addJs("{$this->con_name}.js");
		}
	}

	/**
	 * JSON形式での出力を行います。
	 *
	 * 引数により渡された処理結果boolとデータを配列に格納し、<br />
	 * json_encodeによりJSON形式に変換してそのまま出力します。
	 *
	 * @access protected
	 * @param bool $succeeded JSON受信側でチェックする処理結果
	 * @param bool $data JSON受信側で受け取るデータ
	 */
	protected function outputJson(bool $succeeded, array $data) {
		// Content-Typeをtext/javascriptに設定する。
		Common::setJsonHeaders();
		$json = array(
			'succeeded' => $succeeded,
			'data' => $data,
		);
		echo json_encode($json);
	}

}
