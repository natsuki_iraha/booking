<?php

/**
 * カレンダー画面のコントローラー
 * 
 * カレンダー画面（予約一覧）表示、予約データ取得・変更・削除
 * 
 * @access public
 */
class IndexController extends AbstractController {

	/**
	 * コンストラクタ
	 *
	 * Dispathcer->dispatch() によりインスタンス化されます。<br />
	 * リクエストパラメータのController名とAction名をプロパティにセットします。
	 *
	 * @access protected
	 * @param string $con_name リクエストパラメータのController名
	 * @param string $act_name リクエストパラメータのAction名
	 */
	public function __construct(string $con_name, string $act_name) {
		parent::__construct($con_name, $act_name);
	}

	/**
	 * カレンダー画面（予約一覧）表示
	 *
	 * 以下を行ってViewに渡してレンダリングします。<br />
	 * ・プルダウン用データの取得<br />
	 * ・予約対象の設定<br />
	 * ・表示年月の設定<br />
	 * ・表示年月内の予約データ取得
	 *
	 * @access public
	 * @param array $params 他のControllerやActionから呼ばれた際の追加情報
	 */
	public function indexAction(array $params = null) {
		global $config;
		$this->setupView();
		if ($params !== null) {
			foreach ($params as $key => $params) {
				$this->view->assign($key, $params);
			}
		}

		// app.iniからの設定をViewに渡す
		$this->view->assign('settings', $config['application']);

		// 予約対象プルダウンのデータを取得
		$select_options = array();
		$dao = Db::getDao('BookingTarget');
		$select_options['booking_target'] = $dao->getDataList();
		$this->view->assign('select_options', $select_options);

		// この画面の予約対象を設定
		$target_code = Common::getParam('tc');
		if ($target_code == '') {
			$target_code = $select_options['booking_target'][0]['code'];
		}
		$this->view->assign('target_code', $target_code);

		// この画面の表示年月を設定
		$target_yyyymm = Common::getParam('tm');
		$target_year = Common::left($target_yyyymm, 4);
		$target_month = Common::right($target_yyyymm, 2);
		if ((preg_match('/^\d{6}$/', $target_yyyymm) !== 1) ||
			($target_year - 0 < 2000) || ($target_year - 0 > 2030) ||
			($target_month - 0 < 1) || ($target_month - 0 > 12)) {
			$target_year = date('Y');
			$target_month = date('m');
		}
		$start_date = strtotime($target_year . '/' . $target_month . '/01');
		$this->view->assign('start_date', $start_date);

		// 予約データを取得
		$start_datetime = date('Y/m/d 00:00', $start_date);
		$end_datetime = date('Y/m/d 23:59', strtotime('last day of', $start_date));
		$dao = Db::getDao('BookingData');
		$this->view->assign('booking_data', $dao->getBookingDataList($target_code, $start_datetime, $end_datetime));

		//view
		$this->view->render();
	}

	/**
	 * 予約登録（ajax）
	 *
	 * 「予約」ボタンによりajaxで送られたデータをbooking_dataに登録します。<br />
	 * 結果はjson形式にして出力します。
	 *
	 * @access public
	 */
	public function inputAction() {
		// 受信データをinsert処理用の配列にセット
		$data = array(
			'target_code' => Common::getParam('tc'),
			'id' => Common::getParam('id'),
			'update_datetime' => Common::getParam('update_datetime'),
			'start_datetime' => Common::getParam('start_datetime'),
			'end_datetime' => Common::getParam('end_datetime'),
			'title' => Common::getParam('title'),
			'user_name' => Common::getParam('user_name'),
			'user_section_name' => Common::getParam('user_section_name'),//【追記】2019/03/22　伊良波
			'user_phone_number' => Common::getParam('user_phone_number'),//【追記】2019/03/22　伊良波
			'member_count' => Common::getParam('member_count'),//		  【追記】2019/03/22　伊良波
			'description' => Common::getParam('description'),
			'deleted_flag' => '0',//                              		  【追記】2019/03/22　伊良波
		);
		// 登録時のエラーは$messageに格納される
		$dao = Db::getDao('BookingData');
		$message = '';
		$result = $dao->inputData($data, $message);

		$this->outputJson(($result !== false), array('message' => $message));
	}
	/**
	 * 予約キャンセル（ajax）
	 *
	 * 「削除」ボタンによりajaxで送られたデータについて、<br />
	 *	booking_dataのdeleted_flagを「"1"」にします。<br />
	 * 結果はjson形式にして出力します。
	 *
	 * @access public
	 * 
	 * 【追加】2019/03/25 伊良波
	 */
	public function cancelAction() {
		// 受信データをupdate処理用の配列にセット
		$data = array(
			'target_code' => Common::getParam('tc'),
			'id' => Common::getParam('id'),
			'update_datetime' => Common::getParam('update_datetime'),
			'deleted_flag' => '1',
		);
		// 登録時のエラーは$messageに格納される
		$dao = Db::getDao('BookingData');
		$message = '';
		$result = $dao->inputData($data, $message);

		$this->outputJson(($result !== false), array('message' => $message));
	}
	
	/**
	 * 予約データ取得（ajax）
	 *
	 * 予約済みデータを表示する際の1件別データをbooking_dataから取得します。<br />
	 * 結果はjson形式にして出力します。
	 *
	 * @access public
	 */
	public function getAction() {
		// 受信データから取得対象idをセット
		$data = array(
			'id' => Common::getParam('id'),
		);

		// 取得時のエラーは$messageに格納される
		$dao = Db::getDao('BookingData');
		$message = '';
		$result = $dao->getData($data, $message);

		$this->outputJson(($result !== false), array('message' => $message, 'data' => $result));
	}

}
