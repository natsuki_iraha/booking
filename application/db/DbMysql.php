<?php

class DbMysql extends Db {

	private const NOW_STRING = "NOW()";

	public function __construct($user, $password, $dbname, $host = '127.0.0.1', $charset = 'utf8') {
		
		$this->pdo = new PDO("mysql:host={$host};dbname={$dbname}", $user, $password);
		$this->exec("set names {$charset}");
		parent::__construct();
	}

	protected function getNowString(): string {
		return self::NOW_STRING;
	}

	protected function getLastInsertId(string $table): int {
		return $this->pdo->lastInsertId();
	}

}
