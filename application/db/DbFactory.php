<?php

/**
 * DB接続オブジェクトのFactoryクラス
 *
 * 設定ファイルに定義された情報から適切なDB接続オブジェクトを生成する
 *
 * @access public
 */
final class DbFactory {

	private const DB_TYPE_SQLITE = 'sqlite';
	private const DB_TYPE_MYSQL = 'mysql';

	/**
	 * app.iniの設定を元にDB接続オブジェクトのインスタンスを生成する
	 *
	 * @access public static
	 * @param string $dbname 接続するデータベースの設定名
	 * @return Db DB接続オブジェクトのインスタンス
	 */
	private static $db = [];

	/**
	 * app.iniの設定を元にDB接続オブジェクトのインスタンスを生成する
	 *
	 * @access public static
	 * @param string $dbname 接続するデータベースの設定名
	 * @throws RuntimeException データベース接続設定に誤りがあった場合
	 * @return Db DB接続オブジェクトのインスタンス
	 */
	public static function getDb(string $dbname): Db {
		global $config;
		try {
			if (!isset(self::$db[$dbname])) {
				$dbtype = $config["database:{$dbname}"]['type'];
				$dbconfig = $config["database:{$dbname}:{$dbtype}"];
				if ($dbtype === self::DB_TYPE_SQLITE) {
					self::$db[$dbname] = new DbSqlite(
						$dbconfig['path']);
				} elseif ($dbtype === self::DB_TYPE_MYSQL) {
					self::$db[$dbname] = new DbMysql(
						$dbconfig['user'], $dbconfig['password'],
						$dbconfig['dbname'], $dbconfig['host']);
				}
			}
		} catch (Exception $e) {
			throw new DataAccessException('DbFactory', 'getDb', 'データベース接続に失敗しました。', $e);
		}
		if (is_null(self::$db[$dbname])) {
			throw new DataAccessException('DbFactory', 'getDb', 'データベース接続設定に誤りがあります。');
		}
		return self::$db[$dbname];
	}

}
