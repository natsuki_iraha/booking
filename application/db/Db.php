<?php

/**
 * DB管理クラスの抽象クラス
 *
 * Daoのセットアップと管理、SQL発行など
 *
 * @access abstract
 */
abstract class Db {

	private const COMMA = ',';

	protected $pdo = null;
	protected $error = '';

	/**
	 * Daoクラスをクラス名のファイルをインクルードしてインスタンス化します。
	 *
	 * @access public static
	 * @param string $classname インスタンス化するDaoクラス名
	 * @return mixed Daoクラスのインスタンス
	 */
	public static function getDao(string $classname) {
		require_once MODEL_DIR . $classname . '.php';
		return new $classname();
	}

	/**
	 * コンストラクタ
	 *
	 * 子クラスでインスタンス化したPDOにエラーハンドリング方式を設定します。
	 *
	 * @access protected
	 */
	protected function __construct() {
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	/**
	 * デストラクタ
	 *
	 * DB接続オブジェクトへの参照を破棄します。
	 *
	 * @access public
	 */
	public function __destruct() {
		unset($this->pdo);
	}

	/**
	 * 直前のエラーメッセージを返却します。
	 *
	 * @access public
	 * @return string 直前のエラーメッセージ
	 */
	public function getErrorMessage(): string {
		return $this->error;
	}

	/**
	 * トランザクションを開始します。
	 *
	 * @access public
	 * @return bool トランザクション開始の成否
	 * @see transactionEnd
	 */
	public function transactionStart(): bool {
		return $this->pdo->beginTransaction();
	}

	/**
	 * commitもしくはrollBackコマンドによりトランザクションを終了します。
	 *
	 * @access public
	 * @param bool $status トランザクション内の挿入・更新の成否
	 * @return bool トランザクション終了コマンドの成否
	 * @see transactionStart
	 */
	public function transactionEnd(bool $status = true): bool {
		$command = ($status) ? 'commit' : 'rollBack';
		return $this->pdo->$command();
	}

	/**
	 * 渡されたデータをシングルクォーテーションでくくります。
	 *
	 * @access public
	 * @param string $value 処理対象文字列
	 * @return string 処理後文字列
	 */
	public function quote(string $value): string {
		return $this->pdo->quote($value);
	}

	/**
	 * 現在時刻取得SQL文字列を返す抽象メソッドです。
	 *
	 * @access abstract protected
	 * @return string 現在時刻取得SQL文字列
	 */
	abstract protected function getNowString(): string;

	/**
	 * 最後に挿入したレコードのIDを取得します。
	 *
	 * @access abstract protected
	 * @param string $table IDを取得するテーブル名称
	 * @return int 最後に挿入したレコードのID
	 */
	abstract protected function getLastInsertId(string $table): int;

	/**
	 * データを1行挿入します。
	 *
	 * @access public
	 * @param string $table IDを取得するテーブル名称
	 * @return mixed 最後に挿入したレコードのID、失敗した場合はfalse
	 */
	public function insert(string $table, array $fields) {
		$sql = 'insert into ' . $table . ' ';

		$sqlFields = '';
		$sqlValues = '';
		$separator = '';
		foreach ($fields as $field => $value) {
			if ($value === null) {
				$sqlValues .= $separator . 'null';
			} elseif ($value === 'NOW') {
				$sqlValues .= $separator . $this->getNowString();
			} else {
				$sqlValues .= $separator . $value;
			}
			$sqlFields .= $separator . $field;
			$separator = Db::COMMA;
		}

		$sql .= '(' . $sqlFields . ') values (' . $sqlValues . ')';

		$this->transactionStart();
		$result = $this->exec($sql);
		if ($result === 1) {
			$result = $this->getLastInsertId($table);
			$this->transactionEnd(true);
		} else {
			$result = false;
			$this->transactionEnd(false);
		}
		return $result;
	}

	/**
	 * データを更新します。
	 *
	 * @access public
	 * @param string $table 更新するテーブル名称
	 * @param array $fields 更新するフィールド名称と値の配列
	 * @param array $whereFields 更新対象レコードの絞込条件、省略時null
	 * @return mixed 更新した行数、失敗した場合はfalse
	 */
	public function update(string $table, array $fields, array $whereFields = null) {
		$sql = 'update ' . $table . ' set ';

		$sqlFields = '';
		$separator = '';
		foreach ($fields as $field => $value) {
			if ($value === null) {
				$sqlFields .= $separator . $field . ' = ' . 'null';
			} elseif ($value === 'NOW') {
				$sqlFields .= $separator . $field . ' = ' . $this->getNowString();
			} else {
				$sqlFields .= $separator . $field . ' = ' . $value;
			}
			$separator = Db::COMMA;
		}

		$sqlWhereFields = '';
		if (!is_null($whereFields)) {
			$sqlWhereFields = $this->getSqlWhereFields($whereFields);
		}

		$sql .= $sqlFields . $sqlWhereFields;

		$result = $this->exec($sql);
		return $result;
	}

	/**
	 * データを物理削除します。
	 *
	 * @access public
	 * @param string $table 更新するテーブル名称
	 * @param array $whereFields 削除対象レコードの絞込条件
	 * @param string $option 削除SQLに追記するSQLパーツ
	 * @return mixed 更新した行数、失敗した場合はfalse
	 */
	public function delete(string $table, array $whereFields, string $option = '') {
		$sql = 'delete from ' . $table . ' ';

		$sql .= $this->getSqlWhereFields($whereFields);
		$sql .= $option;

		$result = $this->exec($sql);
		return $result;
	}

	/**
	 * データを検索します。
	 *
	 * @access public
	 * @param array $tables 検索するテーブル名称の配列
	 * @param array $fields 取得するフィールド名称の配列
	 * @param array $whereFields 検索対象レコードの絞込条件、省略時null
	 * @param string $option 削除SQLに追記するSQLパーツ
	 * @return mixed 取得したレコード、失敗した場合はfalse
	 */
	public function select(array $tables, array $fields, array $whereFields = null, string $option = '') {
		$sql = 'select ';

		$sqlFields = join(Db::COMMA, $fields);

		$sqlTables = ' from ' . join(Db::COMMA, $tables);

		$sqlWhereFields = '';
		if (!is_null($whereFields)) {
			$sqlWhereFields = $this->getSqlWhereFields($whereFields);
		}

		$sql .= $sqlFields . $sqlTables . $sqlWhereFields . ' ' . $option;

		$result = $this->query($sql);
		return $result;
	}

	/**
	 * フィールド名称と値の配列からwhere句を作成します。
	 *
	 * @access public
	 * @param array $whereFields where句を構成するフィールド名称と値の配列
	 * @return string where句
	 */
	public function getSqlWhereFields(array $whereFields): string {
		$sqlWhereFields = '';
		$separator = ' where ';
		foreach ($whereFields as $field => $value) {
			if ($value === null) {
				$sqlWhereFields .= $separator . $field . ' is null';
			} elseif ($value === '!') {
				$sqlWhereFields .= $separator . $field . ' is not null';
			} else {
				$sqlWhereFields .= $separator . $field . ' = ' . $value;
			}
			$separator = ' and ';
		}
		return $sqlWhereFields;
	}

	/**
	 * SQLを実行してその結果を返却します。
	 *
	 * @access public
	 * @param string $sql 実行するSQL
	 * @return mixed 結果レコード、失敗の場合はfalse
	 */
	public function query(string $sql) {
		$result = false;
		try {
			$result = $this->pdo->query($sql)->fetchAll(PDO::FETCH_NAMED);
		} catch (Exception $e) {
			$this->error = $e->getMessage() . "<br />[ sql : {$sql} ]<br />";
		}
		return $result;
	}

	/**
	 * SQLを実行してその結果を返却します。
	 *
	 * @access public
	 * @param string $sql 実行するSQL
	 * @return mixed 結果行数、失敗の場合はfalse
	 */
	public function exec(string $sql) {
		$result = false;
		try {
			$result = $this->pdo->exec($sql);
		} catch (Exception $e) {
			$this->error = $e->getMessage() . "<br />[ sql : {$sql} ]<br />";
		}
		return $result;
	}

}
