<?php

/**
 * Viewの実体クラス
 * 
 * Controllerにより生成され、Titleやcss/jsの設定や<br />
 * phtmlへのパラメータ受け渡し、テンプレートの読み込みを行う
 * 
 * @access public
 */
class View {

	protected $path;
	protected $title;
	protected $css;
	protected $js;
	protected $con_name;
	protected $act_name;

	/**
	 * コンストラクタ
	 *
	 * リクエストパラメータのController名とAction名や、命名規則外の場合は
	 * Viewファイル名からViewファイルパスをプロパティにセット、
	 * 同時にtitle、css、jsの初期化を行う
	 *
	 * @access public
	 * @param string $con_name リクエストパラメータのController名
	 * @param string $act_name リクエストパラメータのAction名
	 * @param string $other_phtml 命名規則外の場合のViewファイル名、nullable
	 */
	public function __construct(string $con_name, string $act_name, ?string $other_phtml) {
		$phtml = (is_null($other_phtml)) ? $act_name : $other_phtml;
		$this->path = VIEW_DIR . "{$con_name}/{$phtml}.phtml";
		$this->title = APP_TITLE;
		$this->css = array();
		$this->js = array();
		$this->con_name = $con_name;
		$this->act_name = $act_name;
	}

	/**
	 * HTMLに設定するタイトルをプロパティにセット
	 *
	 * @access public
	 * @param string $value 設定するタイトル
	 */
	public function setTitle(string $value) {
		$this->title = $value;
	}

	/**
	 * HTMLに設定するcssパスをプロパティに追加
	 *
	 * @access public
	 * @param string $path 設定するcssパス
	 */
	public function addCss(string $path) {
		array_push($this->css, $path);
	}

	/**
	 * HTMLに設定するjsパスをプロパティに追加
	 *
	 * @access public
	 * @param string $path 設定するjsパス
	 */
	public function addJs(string $path) {
		array_push($this->js, $path);
	}

	/**
	 * Viewのphtmlに受け渡すデータをプロパティにセット
	 *
	 * path,title,css,jsは使用済みプロパティのためセット不可
	 * 
	 * @access public
	 * @param string $key 設定するデータのキー
	 * @param string $value 設定するデータ
	 */
	public function assign($key, $value) {
		if (('' != $key) && ('title' != $key) && ('path' != $key) && ('css' != $key) && ('js' != $key)) {
			$this->$key = $value;
		} else {
			Throw new Exception('[path,title,css,js]は予約されているためassignできません！');
		}
	}

	/**
	 * テンプレートを読み込む
	 *
	 * テンプレート側ではプロパティ$pathに設定されたphtmlを読み込み<br />
	 * title、css、jsおよびassignしたデータを埋め込む
	 * 
	 * @access public
	 * @see views/template.phtml
	 */
	public function render() {
		require_once VIEW_DIR . 'template.phtml';
	}

}
