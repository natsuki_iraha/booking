<?php

/**
 * URLのクエリストリングやPOSTデータの内容を元に<br />
 * 実際の処理を行うControllerのActionに処理を引き継ぎます。
 *
 * @access public
 */
class Dispatcher {

	/**
	 * リクエストパラメータを取得してcallControllerActionへ渡します。
	 *
	 * パラメータ「c」「a」を取得し、存在しない場合はそれぞれ「index」を呼び出します。
	 *
	 * @access public
	 * @see callControllerAction
	 */
	public function dispatch() {
		$c = Common::getParam('c', 'index');
		$a = Common::getParam('a', 'index');
		self::callControllerAction($c, $a);
	}

	/**
	 * パラメータで指定したControllerクラスを読み込んでActionを呼び出します。
	 *
	 * ControllerやActionが存在しない場合はINDEX_URLへリダイレクトします。
	 *
	 * @access public static
	 * @param string $c 呼び出すController名
	 * @param string $a 呼び出すAction名
	 * @param array $params Actionに渡したいパラメータの配列、省略時null
	 */
	public static function callControllerAction(string $c, string $a, array $params = null) {
		$className = ucfirst(strtolower($c)) . 'Controller';
		$classPath = CTRL_DIR . $className . '.php';
		$actionMethod = strtolower($a) . 'Action';

		// file_existsを使用してControllerクラスファイルの存在確認を行っています。
		if (file_exists($classPath)) {
			require_once $classPath;
			$controllerInstance = new $className($c, $a);
		} else {
			header('LOCATION:' . INDEX_URL);
			exit;
		}

		// method_existsを使用してAction名に対応するメソッドの存在確認を行なっています。
		if (method_exists($controllerInstance, $actionMethod)) {
			$controllerInstance->$actionMethod($params);
		} else {
			header('LOCATION:' . INDEX_URL);
			exit;
		}
	}

}
