<?php

/**
 * Daoクラスの抽象クラス
 *
 * Daoとしての基本的なふるまいを定義しています。
 *
 * @access abstract
 */
abstract class AbstractDao {

	protected $db;
	protected $table_name;

	/**
	 * コンストラクタ
	 *
	 * DB接続オブジェクトを生成してプロパティへセットします。<br />
	 * テーブル名称もプロパティへセットします。
	 *
	 * @access protected
	 * @param string $table_name 使用するテーブルの名称
	 * @param string $db_name 接続するDBの名称、省略時DB_DEFAULT
	 */
	protected function __construct(string $table_name, string $db_name = DB_DEFAULT) {
		$this->db = DbFactory::getDb($db_name);
		$this->table_name = $table_name;
	}

	/**
	 * データ1件セレクト
	 *
	 * 一意に特定できるフィールドの値で検索してデータを1件取得します。
	 *
	 * @access public
	 * @param string $id 取得するデータを一意に検索できる値
	 * @param string $field_name $idを検索するフィールド名称、省略時id
	 * @return array 取得したデータの先頭１件
	 */
	public function getDataById(string $id, string $field_name = 'id'): array {
		$data = $this->db->select(array($this->table_name), array('*'),
			array($field_name => $this->db->quote($id)), 'limit 1');
		return ($data) ? $data[0] : null;
	}

	/**
	 * データ複数件セレクト
	 *
	 * データをソートして全件取得します。<br />
	 * $whereFieldsを指定した絞り込みが可能です。
	 *
	 * @access public
	 * @param bool $desc $order_keyに指定した列のソート順、trueで降順、省略時falseで昇順
	 * @param array $whereFields 絞り込み検索条件、省略時null
	 * @param string $order_key ソートするフィールド名称、省略時id
	 * @return array 取得したデータの配列
	 */
	public function getDataList(bool $desc = false, array $whereFields = null, string $order_key = 'id') {
		$desc = ($desc) ? "order by {$order_key} desc" : "order by {$order_key} asc";
		return $this->db->select(array($this->table_name), array('*'), $whereFields, $desc);
	}

}
