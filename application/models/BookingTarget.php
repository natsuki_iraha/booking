<?php

/**
 * BookingTargetにアクセスするDaoクラス
 *
 * @access public
 */
class BookingTarget extends AbstractDao {

	/**
	 * コンストラクタ
	 *
	 * テーブル名を設定して抽象クラスのコンストラクタを呼び出します。
	 *
	 * @access public
	 */
	public function __construct() {
		parent::__construct('booking_target');
	}

}
