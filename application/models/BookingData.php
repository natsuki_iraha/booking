<?php

/**
 * BookingDataにアクセスするDaoクラス
 *
 * @access public
 */
class BookingData extends AbstractDao {

	/**
	 * コンストラクタ
	 *
	 * テーブル名を設定して抽象クラスのコンストラクタを呼び出します。
	 *
	 * @access public
	 */
	public function __construct() {
		parent::__construct('booking_data');
	}

	/**
	 * 期間内に存在するすべての予約データを取得します。 
	 *
	 * 期間開始日時～期間終了日時の範囲に含まれる、未削除の予約データを検索します。<br />
	 * ソート順は予約開始日時と予約終了日時の昇順です。<br />
	 * 範囲の検索条件は<b>下記①と⑤以外</b>となります。<br />
	 * <br />
	 * 　①【Ｘ】予約開始日時と予約終了日時が、いずれも期間開始日時以前<br />
	 * 　②【Ｏ】予約開始日時が期間開始日時以前で、かつ予約終了日時は期間内<br />
	 * 　③【Ｏ】予約開始日時が期間内で、かつ予約終了日時も期間内<br />
	 * 　④【Ｏ】予約開始日時が期間内で、かつ予約終了日時は期間終了日時以降<br />
	 * 　⑤【Ｘ】予約開始日時と予約終了日時が、いずれも期間終了日時以降<br />
	 * 　⑥【Ｏ】予約開始日時が期間開始日時以前で、かつ予約終了日時は期間終了日時以降<br />
	 * <br />
	 * 期間　　　　ＯＯＯＯＯＯＯＯ<br />
	 * ①　ＸＸＸＸ<br />
	 * ②　　　ＸＸＯＯ<br />
	 * ③　　　　　　　ＯＯＯＯ<br />
	 * ④　　　　　　　　　　　ＯＯＸＸ<br />
	 * ⑤　　　　　　　　　　　　　ＸＸＸＸ<br />
	 * ⑥　ＸＸＸＸＯＯＯＯＯＯＯＯＸＸＸＸ
	 *
	 * @access public
	 * @param string $target_code 予約対象のコード
	 * @param string $start_datetime 期間開始日
	 * @param string $end_datetime 期間終了日
	 * @return array 取得したデータの配列
	 */
	public function getBookingDataList(string $target_code, string $start_datetime, string $end_datetime): array {
		$sql = "select * from {$this->table_name}";
		$sql .= " where target_code='{$target_code}'";
		$sql .= " and not ((start_datetime <= '{$start_datetime}' and end_datetime <= '{$start_datetime}')";
		$sql .= " or (start_datetime >= '{$end_datetime}' and end_datetime >= '{$end_datetime}'))";
		$sql .= " and deleted_flag = '0'";//【追記】未削除のデータを探す 2019/03/25 伊良波		
		$sql .= " order by start_datetime , end_datetime;";

		return $this->db->query($sql);
	}

	/**
	 * 配列内のnull以外のデータをシングルクォーテーションでくくります。
	 * 
	 * @access private
	 * @param array $data 処理対象の配列
	 * @return array 処理後の配列
	 */
	private function quotes(array $data): array {
		foreach ($data as & $item) {
			if (!is_null($item)) {
				$item = $this->db->quote($item);
			}
		}
		return $data;
	}

	/**
	 * 予約データが存在すれば更新、存在しなければ新規登録します。
	 * 
	 * @access public
	 * @param array $data 処理用データ
	 * @param string &$message メッセージを受け渡す領域
	 * @return bool 処理の成否
	 */
	public function inputData(array $data, string &$message): bool {
		$result = false;
		$start_datetime = date('Y/m/d H:i', strtotime($data['start_datetime']));
		$end_datetime = date('Y/m/d H:i', strtotime($data['end_datetime']));
		$datalist = self::getBookingDataList($data['target_code'], $start_datetime, $end_datetime);

		if ((count($datalist) >= 2) || ((count($datalist) == 1) && ($datalist[0]['id'] != $data['id']))) {
			$message = '他の予約と利用日時が重複しています。';
		} else {
			if ($data['id'] != '') {

				$data['update_datetime'] = date('Y/m/d H:i:s');
				$data = $this->quotes($data);
				$result = $this->db->update($this->table_name,
					$data,
					array(
						'id' => $data['id'],
				));
//				【追加】削除時のメッセージ↓
				if ($result !== true) {
					if ($data['deleted_flag'] === "'0'"){
						$message = '予約内容を変更しました。';						
					} else {
						$message ='予約内容をキャンセルしました。';						
					}
				}
//				【追加】削除時のメッセージ↑　2019/03/26 伊良波			
			} else {
				unset($data['id']);
				$data['update_datetime'] = date('Y/m/d H:i:s');
				$data = $this->quotes($data);
				$result = $this->db->insert($this->table_name, $data);
				if ($result !== false) {
					$message = '予約が完了しました。';
				}
			}
			if ($result === false) {
				$message = '処理に失敗しました。最初から再実行してください。';
			}
		}
		return $result;
	}

	/**
	 * 予約データの取得を行います。
	 * 
	 * @access public
	 * @param array $data 処理用データ
	 * @param string &$message メッセージを受け渡す領域
	 * @return mixed 取得できた場合は結果の配列、失敗の場合はfalse
	 */
	public function getData(array $data, string &$message) {
		$result = false;
		$row = $this->getDataById($data['id']);
		if ($row != null) {
			$result = $row;
		}
		return $result;
	}

}
